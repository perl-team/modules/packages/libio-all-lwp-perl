Source: libio-all-lwp-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Florian Schlichting <fsfs@debian.org>
Section: perl
Testsuite: autopkgtest-pkg-perl
Priority: optional
Build-Depends: debhelper-compat (= 12)
Build-Depends-Indep: perl,
                     libio-all-perl,
                     libwww-perl,
                     libtest-pod-perl
Standards-Version: 4.1.4
Vcs-Browser: https://salsa.debian.org/perl-team/modules/packages/libio-all-lwp-perl
Vcs-Git: https://salsa.debian.org/perl-team/modules/packages/libio-all-lwp-perl.git
Homepage: https://metacpan.org/release/IO-All-LWP

Package: libio-all-lwp-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends},
         perl,
         libio-all-perl,
         libwww-perl
Description: Perl module to use HTTP and FTP URLs with IO::All
 IO::All::LWP acts as glue between IO::All and LWP, so that files can
 be read and written through the network using the convenient IO:All
 interface. Note that this module is not used directly: you just use
 IO::All, which knows when to autoload IO::All::HTTP, IO::All::HTTPS,
 IO::All::FTP, or IO::All::Gopher, which implement the specific
 protocols based on IO::All::LWP.
